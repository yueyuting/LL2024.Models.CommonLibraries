﻿using System;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;

namespace LL2024.Models.CommonLibraries
{
    /// <summary>
    /// 通用返回信息类
    /// </summary>
    public class MessageModel<T>
    {
        /// <summary>
        /// 状态码
        /// </summary>
        [JsonPropertyName("status")]
        public int Status { get; set; } = 0;

        /// <summary>
        /// 返回信息
        /// </summary>
        [JsonPropertyName("msg")]
        public string Msg { get; set; } = "";
        /// <summary>
        /// 返回数据集合
        /// </summary>
        [JsonPropertyName("data")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public T Data { get; set; }

        /// <summary>
        /// 跟踪 ID
        /// </summary>
        [JsonPropertyName("traceId")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string TraceId { get; set; }


        /// <summary>
        /// 返回成功
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="msg">消息</param>
        /// <param name="trace">跟踪ID</param>
        /// <returns></returns>
        public static MessageModel<T> Ok(T data = default, string msg = "ok", string trace = null)
        {
            return Message(0, msg, data, trace);
        }

        /// <summary>
        /// 返回成功
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="msg">消息</param>
        /// <param name="trace">跟踪ID</param>
        /// <returns></returns>
        public static MessageModel<T> Success(T data = default, string msg = "ok", string trace = null)
        {
            return Message(0, msg, data, trace);
        }

        /// <summary>
        /// 返回失败
        /// </summary>
        /// <param name="msg">消息</param>
        /// <param name="status">状态</param>
        /// <param name="data">数据</param>
        /// <param name="trace">跟踪ID</param>
        /// <returns></returns>
        public static MessageModel<T> Fail(string msg, int status = -1, T data = default, string trace = null)
        {
            return Message(status, msg, data, trace);
        }

        /// <summary>
        /// 返回失败
        /// </summary>
        /// <param name="msg">消息</param>
        /// <param name="status">状态</param>
        /// <param name="data">数据</param>
        /// <param name="trace">跟踪ID</param>
        /// <returns></returns>
        public static MessageModel<T> Error(string msg, int status = -1, T data = default, string trace = null)
        {
            return Message(status, msg, data, trace);
        }


        /// <summary>
        /// 返回消息
        /// </summary>
        /// <param name="status">状态码，0 表示正确返回</param>
        /// <param name="msg">消息</param>
        /// <param name="data">数据</param>
        /// <param name="trace">跟踪ID</param>
        /// <returns></returns>
        public static MessageModel<T> Message(int status, string msg, T data, string trace = null)
        {
            return new MessageModel<T>() { Msg = msg, Data = data, Status = status, TraceId = trace };
        }

        /// <summary>
        /// 转为 JSON 字符串
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var options = new JsonSerializerOptions { Encoder = JavaScriptEncoder.Create(UnicodeRanges.All) };
            return JsonSerializer.Serialize(this, options);
        }
    }

    public class GenericStringInfo
    {
        public string Info1 { get; set; }
        public string Info2 { get; set; }
        public string Info3 { get; set; }
        public string Info4 { get; set; }
        public string Info5 { get; set; }
        public string Info6 { get; set; }
        public string Info7 { get; set; }
        public string Info8 { get; set; }
        public string Info9 { get; set; }
        public string Info10 { get; set; }
    }

        public static class JsonExtensions
    {
        public static MessageModel<GenericStringInfo> ToMessageModel(this string json)
        {
            return ToMessageModel<GenericStringInfo>(json);
        }

        // 泛型扩展方法
        public static MessageModel<T> ToMessageModel<T>(this string json)
        {
            if (string.IsNullOrWhiteSpace(json))
            {
                // 返回默认值
                return new MessageModel<T>
                {
                    Status = 0, // 或者您定义的一个错误状态码
                    Msg = "Invalid or empty JSON string",
                    Data = default(T), // 使用T类型的默认值
                    TraceId = null
                };
            }

            try
            {
                return JsonSerializer.Deserialize<MessageModel<T>>(json);
            }
            catch (JsonException ex)
            {
                // 如果JSON反序列化失败，也可以返回一个错误信息
                return new MessageModel<T>
                {
                    Status = 400, // 或者其他错误状态码
                    Msg = "Failed to deserialize JSON: " + ex.Message,
                    Data = default(T),
                    TraceId = null
                };
            }
        }

       
    }

}
